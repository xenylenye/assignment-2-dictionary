%define colon_ptr 0

%macro colon 2
    %2: dq colon_ptr
        db %1, 0
    %define colon_ptr %2
%endmacro

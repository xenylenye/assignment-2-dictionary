import subprocess

def run_test_case(test_case):
    process = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate(input=test_case["input"].encode())
    stdout = stdout.decode().strip()
    stderr = stderr.decode().strip()

    return stdout, stderr

tests = [
    {"input": "Java", "expected_output": "Love", "expected_error": ""},
    {"input": "cpp", "expected_output": "death", "expected_error": ""},
    {"input": "rust", "expected_output": "best", "expected_error": ""},
    {"input": "llm", "expected_output": "elp", "expected_error": ""},
    {"input": "cmake", "expected_output": "theworst", "expected_error": ""},
    {"input": "kotlin", "expected_output": "java", "expected_error": ""},
    {"input": "md", "expected_output": "borisdvorkin", "expected_error": ""},
    {"input": "who", "expected_output": "", "expected_error": "Unable to find key"},
    {"input": "asked", "expected_output": "", "expected_error": "Unable to find key"},
    {"input": "this", "expected_output": "", "expected_error": "Unable to find key"},
    {"input": "10010110100110111010101010110101001011001101010101010101010101010101010101010101010101011110101010101010100110101010101010101010101010101010101010101010101010101010101110101010101010101010101010101010101010101010101010101101010101010101110100110101010101010100", "expected_output": "", "expected_error": "Invalid ket value"},
]

failures = 0

for i, test in enumerate(tests):
    stdout, stderr = run_test_case(test)

    if stdout == test["expected_output"] and stderr == test["expected_error"]:
        print(f"很好，测试 {i} 完成")
        print(f"测试下一个键-值对")
    else:
        failures += 1
        print(f"不好，测试 {i} 失败")
        print(f"\t您的输入：{test['input']}")
        print(f"\t预期输出：{test['expected_output']}")
        print(f"\t标准输出：{stdout.strip()}")
        print(f"\t预期错误：{test['expected_error']}")
        print(f"\t标准错误：{stderr.strip()}")
        print(f"\t退出代码：{process.returncode}")

if failures == 0:
    print("所有测试均顺利通过 \n +15 Social Credits")
else:
    print(f"{num_failures} 测试失败")

%include "./colon.inc"

section .data
    colon "Java", key_love
    db "Love", 0

    colon "cpp", key_death
    db "death", 0

    colon "rust", key_best
    db "best", 0

    colon "llm", key_elp
    db "elp", 0

    colon "cmake", key_theworst
    db "theworst", 0

    colon "kotlin", key_java
    db "java", 0

    colon "md", key_dwn
    db "borisdvorkin", 0

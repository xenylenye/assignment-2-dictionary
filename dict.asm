%include "./lib.inc"
%define PTR_SIZE 8
global  find_word
section .text

; 参数；rdi：字符串，rsi：指向数组的指针
; 输出：rax
; 如果未找到 -> rax = 0

find_word:
    push r12
    push r13

    mov  r12, rdi
    mov  r13, rsi
    .loop:
        test rsi, rsi
        jz   .fail

        add  rsi, PTR_SIZE
        mov  rdi, r12
        
        call string_equals
        
        test rax, rax
        jz   .next
        
        mov  rax, r13
        jmp  .exit
    .next:
        mov rsi, [r13]
        mov r13, rsi
        jmp .loop
    .fail:
        xor rax, rax
    .exit:
        pop r13
        pop r12
    ret

%include "./lib.inc"
%include "./words.inc"
%include "./dict.inc"

%define BUFFER_SIZE 255
%define PTR_OFFSET 8
%define ERR_EXIT_CODE 1

global  _start

section .bss
    buffer: resb BUFFER_SIZE ; 为输入单词分配缓冲区, 255个字节

section .text

_start:
    mov rdi, buffer ; RDI 指向缓冲区
    mov rsi, BUFFER_SIZE    ; RSI 为单词最大长度

    call read_word  
    test rax, rax
    jz   .input_error ; 如果返回值为0,跳转到输入错误处理

    mov rdi, buffer
    mov rsi, colon_ptr

    call find_word   ; 在词典中查找输入的单词
    test rax, rax    ; 检查查找结果
    jz   .find_error ; 如果返回值为0,跳转到查找错误处理

    lea  rdi, [rax + PTR_OFFSET] ; RDI 指向找到单词描述的起始地址(偏移8字节)
    call string_length  ; 调用函数计算字符串长度

    add rdi, rax ; 增加RDI,字符串长度
    inc rdi   ; 再加1,为结束符

    call print_string ; 打印找到���字符串
    jmp exit0        ; 退出程序

.input_error:
    mov rdi, input_err_msg ; RDI 指向输入错误信息
    jmp .err_call          ; 跳转到错误处理

.find_error:
    mov rdi, find_err_msg ; RDI 指向查找错误信息

.err_call:
    call print_error ; 打印错误信息
    mov  rdi, ERR_EXIT_CODE      ; RDI 为错误码
    call exit        ; 退出程序

NASM = nasm
NASM_FLAGS = -f elf64
LD = ld
PYTHON = python
SRCS := $(wildcard *.asm)
OBJS := $(SRCS:.asm=.o)
TARGET = main

$(TARGET): $(OBJS)
        $(LD) $(LD_FLAGS) $^ -o $@

main.o: main.asm lib.inc words.inc dict.inc colon.inc

dict.o: dict.asm lib.inc

%.o: %.asm
        $(NASM) $(NASM_FLAGS) $< -o $@

clean:
        rm -rf $(TARGET) $(OBJS)
        rm -rf *.o

run: $(TARGET)
        ./$(TARGET)

test: $(TARGET)
        $(PYTHON) test.py
        $(MAKE) clean

.PHONY: all clean run

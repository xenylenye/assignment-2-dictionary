%define SYSTEM_EXIT  60
%define SYSTEM_WRITE 1
%define SYSTEM_READ  0
%define STDOUT       1
%define STDERR       2
%define BASE_10_DIV  10

%macro WHITESPACE_CHECK 2
    cmp %1, ' '  ; Check for space
    je  %2
    cmp %1, `\t` ; Check for tab
    je  %2
    cmp %1, `\n` ; Check for newline
    je  %2
%endmacro

section .rodata:
    find_err_msg:  db "Unable to find key", 0
    input_err_msg: db "Invalid ket value", 0

global find_err_msg
global input_err_msg

section .text

global  exit
global  exit0
global  string_length
global  string_equals
global  string_copy

global  print_error
global  print_string
global  print_newline
global  print_char
global  print_int
global  print_uint

global  read_char
global  read_word
global  parse_uint
global  parse_int



exit0:
    xor rdi, rdi
exit: 
    mov rax, SYSTEM_EXIT
    syscall

; 接受指向以null结尾的字符串的指针，返回其长度
; 参数：rdi
; 输出：rax
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0 
        je  .end
        inc rax                
        jmp .loop
    .end:
        ret


print_newline:
    mov rdi, `\n`
   

print_char:
    push rdi               
    mov  rax, SYSTEM_WRITE
    mov  rdi, STDOUT       
    mov  rsi, rsp         
    mov  rdx, 1
    syscall
    pop  rdi               
    ret

print_string:
    push rdi
    call string_length    
    mov  rdi, STDOUT      
    pop  rsi              
    mov  rdx, rax         
    mov  rax, SYSTEM_WRITE
    syscall
    ret

print_error:
    push rdi
    call string_length     
    mov  rdi, STDERR       
    pop  rsi               
    mov  rdx, rax          
    mov  rax, SYSTEM_WRITE
    syscall
    ret

print_int:
    test rdi, rdi   
    jns  print_uint 
    push rdi
    mov  rdi, '-'   
    call print_char
    pop  rdi
    neg  rdi        

print_uint:
    push r12

    mov rax,        rdi 
    mov rcx,        BASE_10_DIV  
    mov r12,        1   
    dec rsp
    mov byte [rsp], 0   
    .loop:
        xor  rdx,   rdx 
        div  rcx
        add  dl,    '0' 
        dec  rsp        
        mov  [rsp], dl  
        inc  r12        
        test rax,   rax 
        jne  .loop
    .end:
        mov  rdi, rsp     
        call print_string
        add  rsp, r12    
        pop  r12
        ret

string_equals:
    xor rcx, rcx 
    .loop:
        movzx rax, byte [rdi + rcx] 
        cmp   al,  byte [rsi + rcx] 
        jne   .return_false         
        test  al,  al               
        jz    .return_true
        inc   rcx                   
        jmp   .loop
    .return_true:
        mov rax, 1
        ret
    .return_false:
        xor rax, rax
        ret

read_char:
    xor rdi, rdi         
    mov rax, SYSTEM_READ

    dec rsp      
    mov rsi, rsp 

    mov rdx, 1
    syscall

    test rax, rax
    je   .end

    movzx rax, byte [rsp]
.end:
    inc rsp 
    ret 

read_word:
    push r12
    push r13
    push r14

    mov r12, rdi 
    mov r13, rsi 
    xor r14, r14 

.loop_skip_spaces:
    call read_char

    WHITESPACE_CHECK al, .loop_skip_spaces

    .word_loop:
        test al, al          
        jz   .read_word_done
        
        cmp r14, r13            
        je  .read_word_too_long

        WHITESPACE_CHECK al, .read_word_done

        mov [r12 + r14], al 
        inc r14
        
        call read_char 

        jmp .word_loop

    .read_word_too_long:
        xor rax, rax 
        jmp .end

    .read_word_done:
        cmp r14, r13
        je  .read_word_too_long

        mov rax,            r12 
        mov byte [r12+r14], 0
        mov rdx,            r14 
    .end:
        pop r14
        pop r13
        pop r12
        ret

parse_uint:
    xor rax, rax
    xor rdx, rdx
    .loop:
        movzx rcx, byte [rdi + rdx] 

        test cl, cl 
        jz   .end

        sub cl, '0' 

        cmp cl, 0 
        jl  .end

        cmp cl, 9 
        jg  .end

        imul rax, 10  
        add  rax, rcx 
        inc  rdx

        jmp .loop
    .end:
        ret

parse_int:
    push r12

    xor rdx, rdx
    xor r12, r12

    movzx rcx, byte [rdi] 

    test cl, cl 
    jz   .end

    cmp cl, '-'
    je  .minus

    cmp cl, '+'
    je  .plus

    jmp .parse

    .minus:
        inc r12
    .plus:
        inc rdi

    .parse:
        call parse_uint
        test r12, r12
        jz   .end
        add  rdx, r12
        neg  rax               
    .end:
        pop r12
        ret 

string_copy:
    xor rcx, rcx
    .loop:
        movzx rax,             byte [rdi + rcx] 
        mov   byte[rsi + rcx], al               
        test  al,              al              
        je    .return_length

        inc rcx         
        cmp rcx, rdx
        jb  .loop    
        
        xor rax, rax 
        jmp .end
    .return_length:
        mov rax, rcx
    .end:
        ret
